#include <stdio.h>
#include <stdlib.h>


#define GRID_DIMENTION 10 // warning: Max 26 !
unsigned char gridcontent[GRID_DIMENTION+1][GRID_DIMENTION+1];

int main()
{
    int submarine=3,tir=0,touche=0;
    initgrid();
    placestaticsubmarine();
    showgrid();
   do{

    touche=touche+shoot();
    submarine=submarine-touche;
    touche=0;
    tir++;

    }while(submarine!=0);

    printf("\n\nBravo capitaine! vous avez coule toute la flotte ennemie!\n\n");

    return 0;
}

void initgrid(){
    int y,x;

    printf("   A B C D E F G H I J\n");
    for (y = 1; y <= GRID_DIMENTION; y++){
		printf("%2d ", y);
		for (x = 1; x <= GRID_DIMENTION; x++){
			gridcontent[y][x]='~';
			printf("%c ", gridcontent[y][x]);
			}
	printf("\n");
	}

}
/** \brief shoot - Some elements are from Arben Ferrati and adapted by Theo Gautier.
 *
 * \param
 * \param
 * \return touche
 *
 */
int shoot(){
    int j=-1;
    unsigned int posY=0 , posX=0,touche=0;

    printf("Choisissez ou tirer !:\n\n");

    do{
		printf("Choisissez la position horizontale (A - J): ");
	    scanf("%s", &posX); //Get horizontal axe's position

	    for (j = 0; j <= GRID_DIMENTION; j++){
	    if(posX==j+97  || posX==j+65) posX = j+1;
	    }
	} while(posX < 0 || posX > GRID_DIMENTION);

    showgrid();

    do{
		printf("Choisissez la position vertical (0 - 9):");
	    scanf("%d", &posY); //Get vertical axe's position
	} while(posY < 0 || posY > GRID_DIMENTION);

    if(gridcontent[posY][posX]=='~'){
        gridcontent[posY][posX] = 'R';
        showgrid();
        printf("Rate! votre obus est tombe a la mer !\n\n");


    }

	if(gridcontent[posY][posX] == 254){
        gridcontent[posY][posX] = 'X';
        showgrid();
        printf("Touche!\n\n");
        touche++;


    }

    return touche;
}

/** \brief showgrid - copied from "grid_with_ships" project
 *
 *
 */

void showgrid(){
     int y,x;
    system("cls");
    printf("   A B C D E F G H I J \n");
    for (y = 1; y <= GRID_DIMENTION; y++){
		printf("%2d ", y);
		for (x = 1; x <= GRID_DIMENTION; x++){

			printf("%c ", gridcontent[y][x]);
			}
	printf("\n");
	}
	printf("Legande: X=touche, %c=bateau, R=Tir Rate.\n\n",254);
}


/** \brief placestaticsubmarine - this fonction is only for testing the project. if the project is used in an other project this fonction must be deleted.
 *
 *
 */

 void placestaticsubmarine(){
    gridcontent[5][5]= 254;
    gridcontent[5][6]= 254;
    gridcontent[5][7]= 254;

 }

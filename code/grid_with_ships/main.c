#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
void couleur(int c)
{
        //Couleur du text
        HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
            SetConsoleTextAttribute(H,c);
}
void main(){
    int i;
    system("cls");
    initgrid();

    placeshipfive();
    showgrid();

    placeshipfour();
    showgrid();

    placeshipthree();
    showgrid();

    for(i=1;i<=2;i++){
    placeshiptwo();
    showgrid();
    }

}

/*
    make the grid
*/
#define GRID_DIMENTION 10 // warning: Max 26 !
char gridcontent[GRID_DIMENTION+1][GRID_DIMENTION+1];


/*
    axe y numbers
*/
void initgrid(){
    int y,x;

    printf("   A B C D E F G H I J\n");
    for (y = 1; y <= GRID_DIMENTION; y++){
		printf("%2d ", y);
		for (x = 1; x <= GRID_DIMENTION; x++){
			gridcontent[y][x]='~';
			printf("%c ", gridcontent[y][x]);
			}
	printf("\n");
	}

}

/*
place the 4 cases ships
by Arben Ferati and adapted by Theo Gautier
*/

void placeshipfive(){

    int g,i,j,direction=0,flag=0;

    printf("Bateau a 5 cases:");

    unsigned int posX=0,posY=0;

    do{
		printf("Choisissez la position horizontale (A - J)\n>");
	    scanf("%s", &posX); //Get horizontal axe's position
	    for (j = 0; j <= GRID_DIMENTION; j++){
	    if(posX==j+97  || posX==j+65) posX = j+1;
	    }
	} while(posX < 0 || posX > GRID_DIMENTION);

	do{
		printf("Choisissez la position vertical (0 - 9)\n>");
	    scanf("%d", &posY); //Get vertical axe's position
	} while(posY < 0 || posY > GRID_DIMENTION);

	do{

		flag = 0;

		do
		{
			printf("Choisissez la direction.\n1. A droite\n2. A gauche\n3. En haut\n4. En bas\n>");
	    	scanf("%d", &direction); //Get boat's direction

	    	if (direction == 1){
	    		for (i = 0; i <= 4; i++){

	    			gridcontent[posY][posX+i] = 254;
	    			flag = 0;
	    		}
	    		if (posX+i < 0 || posX+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 4; i++){
	    				gridcontent[posY][posX+i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 2){
	    		for (i = 0; i <= 4; i++){
	    			gridcontent[posY][posX-i] = 254;
	    			flag = 0;
	    		}
	    		if (posX-i < 0 || posX-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 4; i++){
	    				gridcontent[posY][posX-i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 3){
	    		for (i = 0; i <= 4; i++){
	    			gridcontent[posY-i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY-i < 0 || posY-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 4; i++){
	    				gridcontent[posY-i][posX] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 4){
	    		for (i = 0; i <= 4; i++){
	    			gridcontent[posY+i][posX] = 254;
	    			//flag = 0;
	    			flag=0;
	    		}

	    		if (posY+i < 0 || posY+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 4; i++){
	    				gridcontent[posY+i][posX] = '~';
	    			}
	    		}

	    	}
	    }while (direction < 0 || direction > 4);

	}while (flag != 0);

}
void showgrid(){
     int y,x;
    system("cls");
    printf("   A B C D E F G H I J \n");
    for (y = 1; y <= GRID_DIMENTION; y++){
		printf("%2d ", y);
		for (x = 1; x <= GRID_DIMENTION; x++){
            if(gridcontent[y][x] == 254){
            couleur(10);
            }
			printf("%c ", gridcontent[y][x]);
			couleur(7);
			}
	printf("\n");
	}

}

void placeshipfour(){
    printf("Bateau a 4 cases:");
    int g,i,j,direction=0,flag=0;
    unsigned int posX=0,posY=0;
        do{
		printf("Choisissez la position horizontale (A - J)\n>");
	    scanf("%s", &posX); //Get horizontal axe's position
	    for (j = 0; j <= GRID_DIMENTION; j++){
	    if(posX==j+97  || posX==j+65) posX = j+1;
	    }
	} while(posX < 0 || posX > GRID_DIMENTION);

	do{
		printf("Choisissez la position vertical (0 - 9)\n>");
	    scanf("%d", &posY); //Get vertical axe's position
	} while(posY < 0 || posY > GRID_DIMENTION);

	do{

		flag = 0;

		do
		{
			printf("Choisissez la direction.\n1. A droite\n2. A gauche\n3. En haut\n4. En bas\n>");
	    	scanf("%d", &direction); //Get boat's direction

	    	if (direction == 1){
	    		for (i = 0; i <= 3; i++){
	    			gridcontent[posY][posX+i] = 254;
	    			flag = 0;
	    		}
	    		if (posX+i < 0 || posX+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 3; i++){
	    				gridcontent[posY][posX+i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 2){
	    		for (i = 0; i <= 3; i++){
	    			gridcontent[posY][posX-i] = 254;
	    			flag = 0;
	    		}
	    		if (posX-i < 0 || posX-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 3; i++){
	    				gridcontent[posY][posX-i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 3){
	    		for (i = 0; i <= 3; i++){
	    			gridcontent[posY-i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY-i < 0 || posY-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 3; i++){
	    				gridcontent[posY-i][posX] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 4){
	    		for (i = 0; i <= 3; i++){
	    			gridcontent[posY+i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY+i < 0 || posY+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 3; i++){
	    				gridcontent[posY+i][posX] = '~';
	    			}
	    		}
	    	}
	    }while (direction < 0 || direction > 4);

	}while (flag != 0);
}

void placeshipthree(){
    printf("Bateau a 3 cases:");
    int g,i,j,direction=0,flag=0;
    unsigned int posX=0,posY=0;
        do{
		printf("Choisissez la position horizontale (A - J)\n>");
	    scanf("%s", &posX); //Get horizontal axe's position
	    for (j = 0; j <= GRID_DIMENTION; j++){
	    if(posX==j+97  || posX==j+65) posX = j+1;
	    }
	} while(posX < 0 || posX > GRID_DIMENTION);

	do{
		printf("Choisissez la position vertical (0 - 9)\n>");
	    scanf("%d", &posY); //Get vertical axe's position
	} while(posY < 0 || posY > GRID_DIMENTION);

	do{

		flag = 0;

		do
		{
			printf("Choisissez la direction.\n1. A droite\n2. A gauche\n3. En haut\n4. En bas\n>");
	    	scanf("%d", &direction); //Get boat's direction

	    	if (direction == 1){
	    		for (i = 0; i <= 2; i++){
	    			gridcontent[posY][posX+i] = 254;
	    			flag = 0;
	    		}
	    		if (posX+i < 0 || posX+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 2; i++){
	    				gridcontent[posY][posX+i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 2){
	    		for (i = 0; i <= 2; i++){
	    			gridcontent[posY][posX-i] = 254;
	    			flag = 0;
	    		}
	    		if (posX-i < 0 || posX-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 2; i++){
	    				gridcontent[posY][posX-i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 3){
	    		for (i = 0; i <= 2; i++){
	    			gridcontent[posY-i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY-i < 0 || posY-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 2; i++){
	    				gridcontent[posY-i][posX] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 4){
	    		for (i = 0; i <= 2; i++){
	    			gridcontent[posY+i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY+i < 0 || posY+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 2; i++){
	    				gridcontent[posY+i][posX] = '~';
	    			}
	    		}
	    	}
	    }while (direction < 0 || direction > 4);

	}while (flag != 0);
}

void placeshiptwo(){
    printf("Bateau a 2 cases:");
    int g,i,j,direction=0,flag=0;
    unsigned int posX=0,posY=0;
        do{
		printf("Choisissez la position horizontale (A - J)\n>");
	    scanf("%s", &posX); //Get horizontal axe's position
	    for (j = 0; j <= GRID_DIMENTION; j++){
	    if(posX==j+97  || posX==j+65) posX = j+1;
	    }
	} while(posX < 0 || posX > GRID_DIMENTION);

	do{
		printf("Choisissez la position vertical (0 - 9)\n>");
	    scanf("%d", &posY); //Get vertical axe's position
	} while(posY < 0 || posY > GRID_DIMENTION);

	do{

		flag = 0;

		do
		{
			printf("Choisissez la direction.\n1. A droite\n2. A gauche\n3. En haut\n4. En bas\n>");
	    	scanf("%d", &direction); //Get boat's direction

	    	if (direction == 1){
	    		for (i = 0; i <= 1; i++){
	    			gridcontent[posY][posX+i] = 254;
	    			flag = 0;
	    		}
	    		if (posX+i < 0 || posX+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 1; i++){
	    				gridcontent[posY][posX+i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 2){
	    		for (i = 0; i <= 1; i++){
	    			gridcontent[posY][posX-i] = 254;
	    			flag = 0;
	    		}
	    		if (posX-i < 0 || posX-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 1; i++){
	    				gridcontent[posY][posX-i] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 3){
	    		for (i = 0; i <= 1; i++){
	    			gridcontent[posY-i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY-i < 0 || posY-i > GRID_DIMENTION){
	    			flag++;
	    			for (i = 0; i <= 1; i++){
	    				gridcontent[posY-i][posX] = '~';
	    			}
	    		}
	    	}

	    	if (direction == 4){
	    		for (i = 0; i <= 1; i++){
	    			gridcontent[posY+i][posX] = 254;
	    			flag = 0;
	    		}
	    		if (posY+i < 0 || posY+i > GRID_DIMENTION+1){
	    			flag++;
	    			for (i = 0; i <= 1; i++){
	    				gridcontent[posY+i][posX] = '~';
	    			}
	    		}
	    	}
	    }while (direction < 0 || direction > 4);

	}while (flag != 0);
}

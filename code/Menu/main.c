#include <stdio.h>
#include <stdlib.h>

/****************************************
*   Title   :   NavalBattle             *
*   Author  :   Gautier Theo            *
*   Version :   23.03.2018              *
****************************************/


/** \brief  main - start and manage the program
 *
 * \return 0
 */

int main()
{
    int playerChoice= -1;

    menu();
    playerChoice=choiceRequest();
    showChoice(playerChoice);

    return 0;
}

/** \brief  menu - show the menu
 *
 */

void menu(){


    printf("que voulez-vous faire?\n\n");

    printf("1) Aide du jeu\n");
    printf("2) Afficher la liste des scores\n");
    printf("3) s'autentifier\n");
    printf("4) nouvelle partie\n");
    printf("5) Quitter\n");
}

/** \brief choiceRequest - asks the player to make a choice
 *
 * \return return the choice of the player to main()
 */

int choiceRequest(){
    int choice= -1;
    printf("Entrez votre choix  : ");
    scanf("%d",&choice);
    return choice;
}

/** \brief displays the player's choice and launches the corresponding functions
 *
 * \param choice from "playerchoice" in main()
 *
 */

void showChoice(int choice){
    system("cls");
    switch(choice){
    case 1: printf("Aide\n");
            break;

    case 2: printf("liste des scores\n");
            break;

    case 3: printf("s'autentifier\n");
            break;

    case 4: printf("nouvelle partie\n");

    case 5: break;

    default: printf("Erreur! veuillez entrer un nombre valide!\n\n");
            main();
            break;
    }
}
